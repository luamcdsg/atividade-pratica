Sprint 2 - Frontend

1. O TypeScript é uma linguagem de programação desenvolvida pela Microsoft que se baseia no JavaScript. Ele adiciona recursos de tipagem estática, interfaces, classes e outros recursos para facilitar o desenvolvimento de software mais robusto e escalável.

2. Os tipos básicos do TypeScript incluem:
   a. number: para representar números.
   b. string: para representar texto.
   c. boolean: para representar valores verdadeiro ou falso.
   d. object: para representar objetos.
   e. array: para representar listas de valores.
   f. tuple: para representar arrays com tipos específicos em posições fixas.
   g. enum: para representar um conjunto de valores nomeados.
   h. any: para representar qualquer tipo.
   i. void: usado para funções que não retornam valor.
   j. null e undefined: para representar valores nulos ou indefinidos.

3. Uma interface é um conceito fundamental no TypeScript que descreve a estrutura de um objeto. Ela define um conjunto de propriedades e métodos que um objeto deve implementar para ser considerado do tipo da interface. As interfaces são usadas para criar contratos que especificam quais funcionalidades um objeto deve ter.

4. Uma classe é uma estrutura fundamental no TypeScript (e em muitas outras linguagens de programação) que define um tipo de objeto. Classes contêm propriedades e métodos que os objetos desse tipo devem ter. Elas são usadas para criar objetos com comportamento e propriedades específicas.

5. A herança é um conceito em programação orientada a objetos em que uma classe (ou construtor) pode herdar propriedades e métodos de outra classe. Isso permite a reutilização de código e a criação de hierarquias de classes, onde as subclasses herdam as características das classes-pai (superclasses).

6. As vantagens de usar o TypeScript em relação ao JavaScript incluem:
   - Tipagem estática: O TypeScript permite definir tipos para variáveis, ajudando a evitar erros de tipo em tempo de compilação.
   - Maior escalabilidade: O uso de tipos torna o desenvolvimento mais robusto e facilita o trabalho em projetos grandes e complexos.
   - Intellisense: Editores de código como o Visual Studio Code oferecem sugestões de código e informações sobre tipos à medida que você escreve, melhorando a produtividade.
   - Melhor ferramenta de desenvolvimento: O TypeScript fornece recursos avançados de desenvolvimento, como análise estática de código, geração de código eficiente e documentação embutida.
   - Suporte às versões mais recentes do ECMAScript (ES6, ES7, etc.): O TypeScript permite usar as funcionalidades mais recentes do JavaScript.
   - Compatibilidade com JavaScript: O TypeScript é uma extensão do JavaScript, o que significa que você pode usar código JavaScript existente em projetos TypeScript.
   - Maior robustez e legibilidade: O uso de tipos ajuda a documentar e entender o código, facilitando a manutenção e a colaboração em equipe.


Sprint 1 - Frontend

# Atividade Prática

#Questões práticas
#Minhas respostas dos exercícios.

1: Feito 

2: Feito

3:
RESPOSTA: const args = process.argv.slice(2); Esta linha pega os argumentos passados para o script quando ele é executado no terminal. O `process.argv` é um array que contém os argumentos de linha de comando. Usando `.slice(2)`, estamos criando um novo array chamado `args` que contém todos os argumentos a partir do terceiro em diante, ignorando os dois primeiros elementos do array `process.argv`, que são geralmente o caminho para o executável Node.js e o caminho para o arquivo JavaScript que está sendo executado.

console.log(parseInt(args[0]) + parseInt(args[1]));`: Esta linha realiza duas operações:
   parseInt(args[0]) converte o primeiro argumento (índice 0) para um número inteiro, pois os argumentos de linha de comando são tratados como strings por padrão.
   parseInt(args[1]) converte o segundo argumento (índice 1) para um número inteiro da mesma forma.
   - Em seguida, ele realiza a adição dos dois números e usa `console.log` para imprimir o resultado no terminal.

Em resumo, este código calcula a soma de dois números inteiros passados como argumentos de linha de comando e exibe o resultado no console. Por exemplo, se você executar o script com `node meuScript.js 4 5`, ele imprimirá `9`, que é a soma de 5 e 4.



4:

Para adicionar apenas um arquivo ao seu histórico de commit sem usar "git add ."  pode usar o comando "git add nome-do-arquivo". Portanto, o comando seria:

bash
git add calculadora.js


Agora, para fazer um commit com uma mensagem seguindo o estilo "Conventional Commits",  deve escolher um tipo de commit adequado. Para o arquivo "calculadora.js", o tipo de commit pode ser algo como "feat" (para uma nova funcionalidade) ou "fix" (para correção de um problema). Exemplos:

bash
git commit -m "feat: Adiciona funcionalidade de calculadora"


ou

bash
git commit -m "fix: Corrige erro na calculadora"


Para o arquivo "README.md", o tipo de commit pode ser algo como "docs" (para atualizações na documentação) ou "chore" (para tarefas de manutenção). Por exemplo:

bash
git commit -m "docs: Atualiza documentação no README.md"


Por fim, pode fazer um push desse commit usando:

bash
git push


5:

Neste código, você está encapsulando a operação de soma em uma função chamada "soma". Os argumentos são obtidos antes de chamar a função "soma". A função "soma" é definida para calcular a soma dos argumentos e exibi-la no console. Em seguida, você chama a função "soma()" para executar a operação de soma.

A principal diferença é que o código 2 é mais modular, pois a lógica da soma é encapsulada em uma função separada. Isso pode tornar o código mais legível e mais fácil de manter, especialmente se você precisar reutilizar a função "soma()" em outros lugares do seu programa. Além disso, no código 2, você precisa chamar explicitamente a função "soma()" para realizar a operação, enquanto no código 1, a operação é executada imediatamente após a obtenção dos argumentos. Portanto, a escolha entre os dois códigos depende das necessidades específicas do seu programa e da preferência de organização do código

6:

É um programa  que realiza operações de soma e subtração com base nos argumentos da linha de comando:

1. Duas funções são definidas, "soma" e "sub". Cada uma dessas funções calcula e exibe o resultado da soma e da subtração, respectivamente, dos dois primeiros argumentos passados na linha de comando.

2. `const args = process.argv.slice(2);` obtém os argumentos da linha de comando a partir do terceiro argumento em diante (índices 2 em diante) e armazena esses argumentos em uma matriz chamada "args".

3. Um bloco `switch` é usado para verificar o primeiro argumento da linha de comando, que é `args[0]`. Com base no valor desse argumento, o programa decide qual operação deve ser executada. As opções são:

   - Se o primeiro argumento for igual a 'soma', a função "soma()" é chamada, que exibirá a soma dos dois primeiros argumentos.
   - Se o primeiro argumento for igual a 'sub', a função "sub()" é chamada, que exibirá a subtração dos dois primeiros argumentos.
   - Se o primeiro argumento não corresponder a nenhuma das opções anteriores, o programa exibirá 'does not support' seguido do argumento.

Em resumo, este programa permite que o usuário escolha entre realizar uma operação de soma ou subtração com base no primeiro argumento fornecido na linha de comando. 



7: Feito


8:

1. **Garanta que você está na branch de `feature`**: Certifique-se de estar na branch da funcionalidade que você deseja enviar como merge request (por exemplo, `feature/minha-funcionalidade`).

2. **Faça um commit com as alterações**: Certifique-se de ter feito commits locais com suas alterações.

3. **Envie para o repositório remoto**: Use o comando `git push origin feature/minha-funcionalidade` para enviar sua branch com as alterações para o repositório GitLab.

4. **Acesse o GitLab**: Vá para o repositório no GitLab onde deseja criar o merge request.

5. **Crie o Merge Request**:
   - No GitLab, vá até a página principal do repositório.
   - Na barra lateral esquerda, clique em "Merge Requests".
   - Clique no botão "New Merge Request".
   - No campo "Source branch", selecione a sua branch de `feature` (por exemplo, `feature/minha-funcionalidade`).
   - No campo "Target branch", selecione a branch principal (geralmente `main` ou `master`).
   - Adicione um título e uma descrição para o seu merge request, descrevendo o que foi implementado.
   - Clique em "Submit merge request".

6. **Revisão e aprovação**: Os revisores do projeto poderão revisar o código, fazer comentários e aprovar o merge request. Certifique-se de responder a quaisquer comentários e fazer as alterações necessárias, se aplicável.

7. **Merge**: Após a aprovação, um mantenedor do projeto poderá mesclar o seu merge request na branch principal.

9: Feito
 
10: 


var x = args[0];
var y = args[2];
var operator = args[1];

function evaluate(param1, param2, operator) {
  return eval(param1 + operator + param2);
}


Nesta parte,  está atribuindo os valores dos argumentos passados para as variáveis `x`, `y` e `operator`. `x` recebe o primeiro argumento, `y` recebe o terceiro argumento e `operator` recebe o segundo argumento.


if ( console.log( evaluate(x, y, operator) ) ) {}


Nesta parte, está usando uma instrução `if` que envolve uma chamada para `console.log` com o resultado de `evaluate(x, y, operator)`. No entanto, `console.log` sempre retorna `undefined`, então essa condição nunca será verdadeira.

Além disso, o uso da função `eval` para calcular a expressão pode ser perigoso, pois `eval` pode executar qualquer código JavaScript e pode representar uma vulnerabilidade de segurança se os argumentos não forem validados adequadamente.

Se a intenção era imprimir o resultado da expressão calculada, você pode fazer o seguinte:

console.log(evaluate(x, y, operator));


Isso imprimirá o resultado do cálculo no console.

